package com.example.gary.shopifychallenge.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductResponse {
    @SerializedName("products")
    private List<ProductJson> productJsons;

    public List<ProductJson> getProductJsons() {
        return productJsons;
    }
}
