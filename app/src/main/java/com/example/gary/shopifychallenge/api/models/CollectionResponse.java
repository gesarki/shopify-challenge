package com.example.gary.shopifychallenge.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CollectionResponse {

    @SerializedName("custom_collections")
    private List<CustomCollection> customCollections;

    public List<CustomCollection> getCustomCollections() {
        return customCollections;
    }
}
