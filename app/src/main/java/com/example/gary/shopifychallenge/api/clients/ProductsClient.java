package com.example.gary.shopifychallenge.api.clients;

import com.example.gary.shopifychallenge.api.models.ProductResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ProductsClient {

    @GET("/admin/products.json")
    Call<ProductResponse> products(@Query("ids") String productIds, @Query("access_token") String token);
}
