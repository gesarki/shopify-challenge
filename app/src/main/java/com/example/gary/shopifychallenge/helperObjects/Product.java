package com.example.gary.shopifychallenge.helperObjects;

public class Product {
    private String name;
    private static String collectTitle;
    private String imageSrc;
    private int totalInventory;

    public Product(String name, String imageSrc, int totalInventory) {
        this.name = name;
        this.imageSrc = imageSrc;
        this.totalInventory = totalInventory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getCollectTitle() {
        return collectTitle;
    }

    public static void setCollectTitle(String title) {
        collectTitle = title;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public int getTotalInventory() {
        return totalInventory;
    }

    public void setTotalInventory(int totalInventory) {
        this.totalInventory = totalInventory;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", imageSrc='" + imageSrc + '\'' +
                ", totalInventory=" + totalInventory +
                '}';
    }
}
