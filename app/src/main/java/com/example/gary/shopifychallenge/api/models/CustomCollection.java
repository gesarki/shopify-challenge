package com.example.gary.shopifychallenge.api.models;

public class CustomCollection {



    private CollectionImage image;
    private String id;
    private String handle;
    private String title;

    public String getId() {
        return id;
    }

    public String getHandle() {
        return handle;
    }

    public CollectionImage getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

}
