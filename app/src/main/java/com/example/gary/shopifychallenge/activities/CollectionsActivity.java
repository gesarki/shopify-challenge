package com.example.gary.shopifychallenge.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gary.shopifychallenge.R;
import com.example.gary.shopifychallenge.api.models.CollectionResponse;
import com.example.gary.shopifychallenge.api.clients.CollectionsClient;
import com.example.gary.shopifychallenge.api.models.CustomCollection;
import com.example.gary.shopifychallenge.ui.CollectionAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CollectionsActivity extends AppCompatActivity {

    public static final String COLLECTION_ID = "com.example.gary.shopifychallenge.COLLECTION_ID";
    public static final String COLLECTION_TITLE = "com.example.gary.shopifychallenge.COLLECTION_TITLE";

    private ListView listView;

    List<CustomCollection> collections;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collections);

        listView = (ListView) findViewById(R.id.collectionsList);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://shopicruit.myshopify.com")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        CollectionsClient client = retrofit.create(CollectionsClient.class);

        Call<CollectionResponse> call = client.collections("1", "c32313df0d0ef512ca64d5b336a0d7c6");

        call.enqueue(new Callback<CollectionResponse>() {
            @Override
            public void onResponse(Call<CollectionResponse> call, Response<CollectionResponse> response) {
                CollectionResponse res = response.body();
                collections = res.getCustomCollections();
                listView.setAdapter(new CollectionAdapter(CollectionsActivity.this, collections));
            }

            @Override
            public void onFailure(Call<CollectionResponse> call, Throwable t) {
                Toast.makeText(CollectionsActivity.this, "error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(CollectionsActivity.this, DetailsActivity.class);
                        intent.putExtra(COLLECTION_ID, collections.get(position).getId());
                        intent.putExtra(COLLECTION_TITLE, collections.get(position).getTitle());
                        startActivity(intent);
                    }
                }
        );
    }
}
