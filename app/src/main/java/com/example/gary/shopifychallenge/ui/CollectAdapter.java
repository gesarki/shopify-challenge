package com.example.gary.shopifychallenge.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gary.shopifychallenge.R;
import com.example.gary.shopifychallenge.api.models.Collect;
import com.example.gary.shopifychallenge.api.models.CustomCollection;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CollectAdapter extends ArrayAdapter {

    private Context context;
    private List<Collect> collectList;

    public CollectAdapter(Context context, List<Collect> collectList) {
        super(context, R.layout.collection_row, collectList);

        this.context = context;
        this.collectList = collectList;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.collection_row, parent, false);
        }

        TextView textView = (TextView) row.findViewById(R.id.topLine);
        ImageView imageView = (ImageView) row.findViewById(R.id.rowImage);

        Collect item = collectList.get(position);
        String message = item.getProductId();
        textView.setText(message);

        return row;
    }
}
