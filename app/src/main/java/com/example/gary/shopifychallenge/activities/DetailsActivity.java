package com.example.gary.shopifychallenge.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gary.shopifychallenge.R;
import com.example.gary.shopifychallenge.api.clients.CollectionsClient;
import com.example.gary.shopifychallenge.api.clients.CollectsClient;
import com.example.gary.shopifychallenge.api.clients.ProductsClient;
import com.example.gary.shopifychallenge.api.models.Collect;
import com.example.gary.shopifychallenge.api.models.CollectResponse;
import com.example.gary.shopifychallenge.api.models.ProductJson;

import com.example.gary.shopifychallenge.api.models.CollectionResponse;
import com.example.gary.shopifychallenge.api.models.ProductResponse;
import com.example.gary.shopifychallenge.helperObjects.Product;
import com.example.gary.shopifychallenge.ui.CollectAdapter;
import com.example.gary.shopifychallenge.ui.CollectionAdapter;
import com.example.gary.shopifychallenge.ui.ProductAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailsActivity extends AppCompatActivity {

    private ListView listView;

    private List<Product> products;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        System.out.println("came to onCreate DetailsActivity");
        // init products
        products = new ArrayList<>();
        products.clear();

        //set listView
        listView = (ListView) findViewById(R.id.detailsList);

        // get the collectionId from the intent
        Intent intent = getIntent();
        String collectionId = intent.getStringExtra(CollectionsActivity.COLLECTION_ID);

        // set the products' collectTitles
        Product.setCollectTitle(intent.getStringExtra(CollectionsActivity.COLLECTION_TITLE));

        // get all the product_id's in the collection
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://shopicruit.myshopify.com")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        CollectsClient client = retrofit.create(CollectsClient.class);

        Call<CollectResponse> call = client.collects(collectionId, "c32313df0d0ef512ca64d5b336a0d7c6");

        call.enqueue(new Callback<CollectResponse>() {
            @Override
            public void onResponse(Call<CollectResponse> call, Response<CollectResponse> response) {
                CollectResponse res = response.body();
                List<Collect> collects = res.getCollects();
                Log.d("collects", String.valueOf(collects.size()) );
//                listView.setAdapter(new CollectAdapter(DetailsActivity.this, collects));
                nextRequest(collects);
            }

            @Override
            public void onFailure(Call<CollectResponse> call, Throwable t) {
                Toast.makeText(DetailsActivity.this, "error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });


        // get every product's name,
        // total inventory accross all variants,
        // title
        // and image


    }

    public void nextRequest(final List<Collect> collects) {
        String ids = "";
        // get all the productIds
        for (Collect c : collects) {
            ids += (c.getProductId());
            ids += (",");
        }

        ids = ids.substring(0, ids.length()-1);

        // make next request with it
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://shopicruit.myshopify.com")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        ProductsClient client = retrofit.create(ProductsClient.class);

        Call<ProductResponse> call = client.products(ids, "c32313df0d0ef512ca64d5b336a0d7c6");

        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                ProductResponse res = response.body();
                List<ProductJson> productJsons= res.getProductJsons();
                Log.d("number of products", String.valueOf(productJsons.size()));
                Log.d("url", call.request().url().toString());
                for (ProductJson productJson : productJsons) {

                    int totalInv = 0;
                    for (ProductJson.Variant variant : productJson.getVariants()) {
                        totalInv += variant.getInventoryQuantity();
                    }

                    products.add(new Product(productJson.getTitle(), productJson.getImage().getSrc(), totalInv));
                }

                ProductAdapter pa = new ProductAdapter(DetailsActivity.this, products);
                listView.setAdapter(pa);
                pa.notifyDataSetChanged();

                for (Product p : products) {
                    Log.d("debug", p.toString());
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                Toast.makeText(DetailsActivity.this, "error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

    }
}
