package com.example.gary.shopifychallenge.api.clients;

import com.example.gary.shopifychallenge.api.models.CollectionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CollectionsClient {

    @GET("/admin/custom_collections.json")
    Call<CollectionResponse> collections(@Query("page") String pageNum, @Query("access_token") String token);

}
