package com.example.gary.shopifychallenge.api.models;

import java.util.List;

public class CollectResponse {

    private List<Collect> collects;


    public List<Collect> getCollects() {
        return collects;
    }
}
