package com.example.gary.shopifychallenge.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gary.shopifychallenge.R;
import com.example.gary.shopifychallenge.api.models.CustomCollection;

import com.squareup.picasso.*;
import java.util.List;

public class CollectionAdapter extends ArrayAdapter<CustomCollection> {

    private Context context;
    private List<CustomCollection> values;

    public CollectionAdapter(Context context, List<CustomCollection> values) {
        super(context, R.layout.collection_row, values);

        this.context = context;
        this.values = values;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.collection_row, parent, false);
        }

        TextView textView = (TextView) row.findViewById(R.id.topLine);
        ImageView imageView = (ImageView) row.findViewById(R.id.rowImage);

        CustomCollection item = values.get(position);
        String message = item.getTitle();
        textView.setText(message);

        Picasso.get().load(values.get(position).getImage().getSrc()).into(imageView);

        return row;
    }

}
