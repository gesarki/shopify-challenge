package com.example.gary.shopifychallenge.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductJson {
    private String title;
    private ProductImage image;
    private List<Variant> variants;

    public ProductImage getImage() {
        return image;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public String getTitle() {
        return title;
    }

    public class ProductImage {
        private String src;

        public String getSrc() {
            return src;
        }
    }

    public class Variant {

        @SerializedName("inventory_quantity")
        private int inventoryQuantity;


        public int getInventoryQuantity() {
            return inventoryQuantity;
        }
    }


}
