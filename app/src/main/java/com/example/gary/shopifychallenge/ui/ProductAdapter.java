package com.example.gary.shopifychallenge.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gary.shopifychallenge.R;
import com.example.gary.shopifychallenge.api.models.Collect;
import com.example.gary.shopifychallenge.api.models.CustomCollection;
import com.example.gary.shopifychallenge.helperObjects.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductAdapter extends ArrayAdapter {

    private Context context;
    private List<Product> products;

    public ProductAdapter(Context context, List<Product> products) {
        super(context, R.layout.collection_row, products);

        this.context = context;
        this.products = products;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;


        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.collection_row, parent, false);

        TextView topLine = (TextView) row.findViewById(R.id.topLine);
        TextView secondLine = (TextView) row.findViewById(R.id.secondLine);
        TextView thirdLine = (TextView) row.findViewById(R.id.thirdLine);
        ImageView imageView = (ImageView) row.findViewById(R.id.rowImage);

        Product item = products.get(position);
        String name = item.getName();
        String totalInventory = String.valueOf(item.getTotalInventory());
        String collectionTitle = Product.getCollectTitle();

        topLine.setText(name);
        secondLine.setText("Inventory Count: " + String.valueOf(totalInventory));
        thirdLine.setText("Collection: " + collectionTitle);

        Picasso.get().load(item.getImageSrc()).into(imageView);

        return row;
    }
}
