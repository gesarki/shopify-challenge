package com.example.gary.shopifychallenge.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Collect {

    @SerializedName("product_id")
    private String productId;

    public String getProductId() {
        return productId;
    }
}
