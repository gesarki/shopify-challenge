package com.example.gary.shopifychallenge.api.clients;

import com.example.gary.shopifychallenge.api.models.CollectResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CollectsClient {

    @GET("/admin/collects.json?")
    Call<CollectResponse> collects(@Query("collection_id") String collectionId, @Query("access_token") String token);
}
